export const constants = {
  windspeed: 'Windspeed',
  speed: '(m/s)',
  currentWind: '(current)',
  maxWind: '(max)',
  medianWind: '(median)',
  iconSize: 60,
}