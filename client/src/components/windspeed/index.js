import React, { Component } from 'react';
import { WiStrongWind } from "weather-icons-react";
import { constants } from './constants/';
import style from './style.js';

class Windspeed extends Component {

  render() {
    return (
      <div style={style.mainDiv}>
        <h1 style={style.mainTitle}>
          {constants.windspeed}
        </h1>
        <h3 style={style.mainTitle}>
          {constants.speed}
        </h3>
        <div style={style.windspeedDiv}>
          <h1 style={style.dataTitle}>
            {this.props.windspeed}
            <a style={style.designation}>
              {constants.currentWind}
            </a>
          </h1>
        </div>
        <div style={style.windspeedMaxDiv}>
          <h1 style={style.dataTitle}>
            {this.props.windspeedMax}
            <a style={style.designation}>
              {constants.maxWind}
            </a>
          </h1>
        </div>
        <div style={style.windspeedMedian}>
          <h1 style={style.dataTitle}>
            {this.props.windspeedMedian}
            <a style={style.designation}>
              {constants.medianWind}
            </a>
          </h1>
        </div>
        <div style={style.icon}>
          <WiStrongWind size={constants.iconSize} />
        </div>
      </div>
    );
  }
}

export default Windspeed;
