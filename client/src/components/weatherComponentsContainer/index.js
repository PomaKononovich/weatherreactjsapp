import React, { Component } from 'react';
import { get } from 'lodash';
import Temperature from '../temperature';
import Devpoint from '../devpoint';
import Humidity from '../humidity';
import Pressure from '../pressure';
import Windspeed from '../windspeed';
import { constants } from './constants/';
import style from './style.js';

class WeatherReviewer extends Component {

  render() {
    document.title = get(this.props, 'weatherStatistics.temperature') + constants.meteoTitle
    return (
      <div>
        <Temperature
          temperature={get(this.props, 'weatherStatistics.temperature')}
        />
        <Humidity
          humidity={get(this.props, 'weatherStatistics.humidity')}
        />
        <Pressure
          pressure={get(this.props, 'weatherStatistics.pressure')}
          pressureNormal={get(this.props, 'weatherStatistics.pressure_normal')}
        />
        <Devpoint
          devpoint={get(this.props, 'weatherStatistics.devpoint')}
        />
        <Windspeed
          windspeed={get(this.props, 'weatherStatistics.windspeed')}
          windspeedMax={get(this.props, 'weatherStatistics.windspeed_max')}
          windspeedMedian={get(this.props, 'weatherStatistics.windspeed_median')}
        />
        <div onClick={() => this.props.updateAction()} style={style.updateTime}>
          <h1>{constants.lastUpdate} {this.props.updateTime}</h1>
        </div>
      </div>
    );
  }
}

export default WeatherReviewer;
