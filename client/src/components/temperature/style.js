export default {
  div: {
    color: '#e0f7fa',
    width: "50%",
    opacity: '.8',
    textAlign: 'center',
    fontFamily: 'Open Sans',
    fontSize: '100px',
    position: 'fixed',
  },
}