import React, { Component } from 'react';
import { constants } from './constants/';
import style from './style.js';

class Temperature extends Component {

  render() {
    return (
      <div style={style.div}>
        <h1>{this.props.temperature} {constants.celsium}</h1>
      </div>
    );
  }
}

export default Temperature;
