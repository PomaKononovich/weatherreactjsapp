import React, { Component } from 'react';
import { WiHumidity } from "weather-icons-react";
import { constants } from './constants/';
import style from './style.js';

class Humidity extends Component {

  render() {
    return (
      <div style={style.mainDiv}>
        <h1 style={style.mainTitle}>{constants.humidity}</h1>
        <div style={style.dataDiv}>
          <h1 style={style.dataTitle}>{this.props.humidity}</h1>
        </div>
        <div style={style.icon}>
          <WiHumidity size={constants.iconSize} />
        </div>
      </div>
    );
  }
}

export default Humidity;
