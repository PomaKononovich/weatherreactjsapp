import React, { Component } from 'react';
import WeatherReviewer from './weatherComponentsContainer';
import MorningBackground from '../../src/public/morning.jpg';
import DayBackground from '../../src/public/day.jpg';
import EveningBackground from '../../src/public/evening.jpg';
import NightBackground from '../../src/public/night.jpg';
import { constants } from './constants/index.js';
import style from './style.js';

class App extends Component {
  state = {
    weatherData: null,
  };

  componentDidMount() {
    this.callBackendAPI()
      .then(res => {
        localStorage.setItem(constants.weatherStatistics, JSON.stringify(res.weatherStatistics));
        localStorage.setItem(constants.updateTime, new Date().toLocaleString());
        this.setState({
          weatherData: JSON.stringify(res.weatherStatistics),
        })
      })
      .catch(this.localStorageToState);
  }

  localStorageToState = () => {
    this.setState({
      weatherData: localStorage.getItem(constants.weatherStatistics)
    })
  };

  callBackendAPI = async () => {
    const response = await fetch(constants.weatherPath);
    const body = await response.json();
    if (response.status !== 200) {
      throw Error(body.message)
    }
    return body;
  };

  render() {
    if (!this.state.weatherData) { return null; }
    return (
      <div style={style.sectionStyle(`url(${DayBackground})`)}>
        <WeatherReviewer
          weatherStatistics={JSON.parse(this.state.weatherData)}
          updateTime={localStorage.getItem(constants.updateTime)}
          updateAction={this.callBackendAPI}
        />
      </div>
    );
  }
}

export default App;
