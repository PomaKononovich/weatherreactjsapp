import React, { Component } from 'react';
import { WiRaindrops } from "weather-icons-react";
import { WiCelsius } from "weather-icons-react";
import { constants } from './constants/';
import style from './style.js';

class Devpoint extends Component {

  render() {
    return (
      <div style={style.mainDiv}>
        <h1 style={style.mainTitle}>
          {constants.devpoint}
        </h1>
        <div style={style.dataDiv}>
          <h1 style={style.dataTitle}>{this.props.devpoint}</h1>
        </div>
        <div style={style.iconCelsius}>
          <WiCelsius size={constants.celsiusIconSize} />
        </div>
        <div style={style.icon}>
          <WiRaindrops size={constants.iconSize} />
        </div>
      </div>
    );
  }
}

export default Devpoint;
