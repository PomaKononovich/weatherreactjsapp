export default {
  mainDiv: {
    position: 'fixed',
    top: '25%',
    left: '60%',
    height: '150px',
    width: "36%",
    opacity: '.7',
    background: '#ffffff',
    borderRadius: '40px 15px 15px 40px',
  },
  mainTitle: {
    position: 'relative',
    width: '40%',
    left: '50%',
    top: '20%',
    margin: '0',
  },
  dataDiv: {
    textAlign: 'center',
    background: '#b2dfdb',
    height: '40%',
    width: '28%',
    margin: '-65px 20px',
    borderRadius: '40px 15px 15px 40px',
  },
  dataTitle: {
    position: 'relative',
    top: '19%',
    left: '5%',
  },
  icon: {
    position: 'relative',
    width: '40%',
    top: '4%',
    left: '35%',
    margin: '0',
  },
  normalTitle: {
    position: 'relative',
    width: '40%',
    left: '50%',
    top: '10%',
    margin: '0', 
  }
}