import React, { Component } from 'react';
import { WiBarometer } from "weather-icons-react";
import { constants } from './constants/';
import style from './style.js';

class Pressure extends Component {

  render() {
    return (
      <div style={style.mainDiv}>
        <h1 style={style.mainTitle}>
          {constants.pressure}
        </h1>
        <h3 style={style.mainTitle}>
          {constants.designations}
        </h3>
        <div style={style.dataDiv}>
          <h1 style={style.dataTitle}>{this.props.pressure}</h1>
        </div>
        <div style={style.icon}>
          <WiBarometer size={constants.iconSize} />
        </div>
        <h3 style={style.normalTitle}>
          {constants.normalPressure} {this.props.pressureNormal}
        </h3>
      </div>
    );
  }
}

export default Pressure;
