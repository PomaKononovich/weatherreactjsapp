export default {
  sectionStyle: (Background) => ({
    height: '100%',
    width: '100%',
    backgroundImage: Background,
    backgroundSize: '100% 100%',
    backgroundRepeat: 'no-repeat',
    position: 'absolute',
    top: '0px',
    left: '0px',
    fontFamily: 'Open Sans, sans-serif',
  }),
}