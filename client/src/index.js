import React from 'react';
import { render } from 'react-dom';
import App from './components/App';
import Favicon from 'react-favicon';

render(
  <div>
    <Favicon url="/cloud.ico" />
    <App />
  </div>,
  document.getElementById('root'),
)
