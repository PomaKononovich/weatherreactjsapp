const express = require('express');
const app = express();
const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Listening on port ${port}`));

const axios = require('axios');

axios.defaults.headers.common['Authorization'] = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiI2OGJiMmI3ZmFhZjQ0ZjRlYjkxYzc4ZjQ3OGE4YmNmYiIsImlhdCI6MTU1MjQ4MjQzMCwiZXhwIjoxODY3ODQyNDMwfQ.IA4c-uGxqmXnjfGQy3aavF7jfVH3VKRK_L_HyaYvHHw';
axios.defaults.headers.common['Content-Type'] = 'application/json';

const api = axios.create({
	baseURL: 'https://home.mef.space/api/template',
});

app.get('/weather', (req, res) => {
	return api.post('', {
		"template": "{\n\"temperature\" : \"{{states('sensor.meteo_station_temperature')}}\",\n\"humidity\" : \"{{states('sensor.meteo_station_humidity')}}\",\n\"pressure\" : \"{{states('sensor.pressure_fact')}}\",\n\"pressure_normal\" : \"{{states('sensor.normal_pressure')}}\",\n\"devpoint\":\"{{states('sensor.meteo_station_dev_point')}}\",\n\"windspeed\":\"{{states('sensor.meteo_station_wind')}}\",\n\"windspeed_max\":\"{{states('sensor.meteo_station_wind_max')}}\",\n\"windspeed_median\":\"{{states('sensor.meteo_station_wind_median')}}\"\n}"
	})
		.then((response) => {
			res.send({ weatherStatistics: response.data })
		})
		.catch(err => {
			console.log(err)
		})
});
